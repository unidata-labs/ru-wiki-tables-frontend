# ru-wiki-tables-frontend



Russian Web Tables (RWT) toolkit is a modern light-weight toolkit for extraction of tables from Wikipedia. It is a light-weight, full cycle (includes web crawler), and customizable. The source code for its backend and frontend can be found in the following repositories:
1) https://gitlab.com/unidata-labs/ru-wiki-tables-backend
2) https://gitlab.com/unidata-labs/ru-wiki-tables-frontend

The extracted corpus can be found here:

https://gitlab.com/unidata-labs/ru-wiki-tables-dataset
If you are going to use the corpus or the toolkit, please cite the following paper:
Platon Fedorov, Alexey Mironov, and George Chernishev. Russian Web Tables: A Public Corpus of Web Tables for Russian Language Based on Wikipedia. DAMDID'22 (accepted).
